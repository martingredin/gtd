﻿using Gtd.Infrastructure.Data;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gtd.Test
{
	[TestFixture]
    public class GtdContextTests
    {
		[Test]
		public void CreateDb()
		{
			using (var ctx = new GtdContext())
			{
				ctx.Database.Initialize(false);
			}
		}
    }
}
