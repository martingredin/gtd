﻿using Gtd.Core.Model;
using Gtd.Core.Services;
using Gtd.Infrastructure.Data;
using Gtd.WebServices.Services;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Gtd.Test.Services
{
	[TestFixture]
	public class UserServiceTests : TestBase
	{
		[SetUp]
		public void Init()
		{
			UserInit();
			userService = new UserService(mockContext.Object);
		}

		[Test]
		public void Authenticate_ExistingUser_ReturnsTrue()
		{
			var res = userService.AuthenticateUser("Martin", "abc123");

			Assert.IsTrue(res);
		}

		[Test]
		public void CreateUser_NewUser_ReturnsTrue()
		{
			var res = userService.RegisterUser("Olle", "efg567", "efg567");

			Assert.IsTrue(res);
			userMockSet.Verify(m => m.Add(It.IsAny<User>()), Times.Once());
			mockContext.Verify(m => m.SaveChanges(), Times.Once()); 
		}

		[Test]
		public void CreateUser_ExistingUser_ReturnsFalse()
		{
			var res = userService.RegisterUser("Martin", "abc123", "abc123");

			Assert.IsFalse(res);
			userMockSet.Verify(m => m.Add(It.IsAny<User>()), Times.Never());
			mockContext.Verify(m => m.SaveChanges(), Times.Never());
		}

		[Test]
		public void CreateUser_FaultyPassword_ReturnsFalse()
		{
			var res = userService.RegisterUser("Olle", "efg567", "ghi789");

			Assert.IsFalse(res);
			userMockSet.Verify(m => m.Add(It.IsAny<User>()), Times.Never());
			mockContext.Verify(m => m.SaveChanges(), Times.Never());
		}

		[Test]
		public void GetUser_WithTasks()
		{
			var res = userService.GetUser("Martin");
		}
	}
}