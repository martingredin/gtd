﻿using Gtd.WebServices.Services;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gtd.Test.Services
{
	[TestFixture]
	public class StateServiceTests : TestBase
	{
		[SetUp]
		public void Init()
		{
			StateInit();
			stateService = new StateService(mockContext.Object);
		}

		[Test]
		public void GetState_ById_ReturnsTaskWithId()
		{
			var state = stateService.GetState(1);

			Assert.AreEqual(state, stateData.FirstOrDefault(s => s.Id == 1));
		}

		[Test]
		public void GetAllStates_ReturnsAllStatesInList()
		{
			var states = stateService.GetAllStates();

			Assert.AreEqual(states.Count(), stateData.Count());
		}
	}
}
