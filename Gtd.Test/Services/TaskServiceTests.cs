﻿using Gtd.Core.Model;
using Gtd.Core.Services;
using Gtd.Infrastructure.Data;
using Gtd.WebServices.Services;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gtd.Test.Services
{
	[TestFixture]
	public class TaskServiceTests : TestBase
	{
		[SetUp]
		public void Init()
		{
			UserInit();
			TaskInit();
			StateInit();
			taskService = new TaskService(mockContext.Object);
		}

		[Test]
		public void AddTask_TaskAddedAndSaveChanges_EqualsOnce()
		{
			taskService.AddTask("Martin", "test task", "make sure that adding tasks works", new DateTime(2015, 01, 10), new DateTime(2015, 05, 15));

			taskMockSet.Verify(m => m.Add(It.IsAny<GtdTask>()), Times.Once());
			mockContext.Verify(m => m.SaveChanges(), Times.AtLeastOnce()); 
		}

		[Test]
		public void UpdateTask_SaveChanges_EqualsOnce()
		{
			taskService.UpdateTask(1, "updated test task", "make sure updating tasks work");

			mockContext.Verify(m => m.SaveChanges(), Times.Once()); 
		}

		[Test]
		public void MoveTaskToStateForwards_InitialState_ShouldMoveToAnalyzed()
		{
			taskService.MoveTaskToState(0, Direction.Forwards);

			Assert.AreEqual(taskData.FirstOrDefault(i => i.Id == 0).KanbanStateId, analyzedState.Id);
		}

		[Test]
		public void MoveTaskToStateBackwards_FinishedState_ShouldMoveToInProgress()
		{
			taskService.MoveTaskToState(2, Direction.Backwards);

			Assert.AreEqual(taskData.FirstOrDefault(i => i.Id == 2).KanbanStateId, inProgressState.Id);
		}

		[Test]
		public void ChangeTaskToState_BlaBla_ShouldMoveThere()
		{
			taskService.ChangeTaskToState(2, analyzedState.Id);

			Assert.AreEqual(taskData.FirstOrDefault(i => i.Id == 2).KanbanStateId, analyzedState.Id);
		}

		[Test]
		public void GetTask_CheckState()
		{
			var task = taskService.GetTask(1);
		}
	}
}