﻿using Gtd.Core.Model;
using Gtd.Core.Services;
using Gtd.Infrastructure.Data;
using Gtd.WebServices.Services;
using Moq;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Gtd.Test.Services
{
	public class TestBase
	{
		protected Mock<DbSet<GtdTask>> taskMockSet = new Mock<DbSet<GtdTask>>();
		protected Mock<DbSet<KanbanState>> stateMockSet = new Mock<DbSet<KanbanState>>();
		protected Mock<DbSet<User>> userMockSet = new Mock<DbSet<User>>();
		protected Mock<GtdContext> mockContext = new Mock<GtdContext>();

		protected IQueryable<GtdTask> taskData;

		protected KanbanState newState = new KanbanState { Name = "New" };
		protected KanbanState analyzedState = new KanbanState { Name = "Analyzed" };
		protected KanbanState inProgressState = new KanbanState { Name = "In progress" };
		protected KanbanState finishedState = new KanbanState { Name = "Finished" };
		protected IQueryable<KanbanState> stateData;

		protected IQueryable<User> userData;

		protected IUserService userService;
		protected ITaskService taskService;
		protected IStateService stateService;

		protected void UserInit()
		{
			RNGCryptoServiceProvider rngCryptoSP = new RNGCryptoServiceProvider();
			byte[] saltArr = new byte[10];

			rngCryptoSP.GetBytes(saltArr);
			var salt1 = Convert.ToBase64String(saltArr);
			rngCryptoSP.GetBytes(saltArr);
			var salt2 = Convert.ToBase64String(saltArr);

			userData = new List<User>()
				{
					User.Create("Martin", "abc123", salt1),
					User.Create("Kalle", "cde345", salt2)
				}.AsQueryable();

			userMockSet.As<IQueryable<User>>().Setup(m => m.Provider).Returns(userData.Provider);
			userMockSet.As<IQueryable<User>>().Setup(m => m.Expression).Returns(userData.Expression);
			userMockSet.As<IQueryable<User>>().Setup(m => m.ElementType).Returns(userData.ElementType);
			userMockSet.As<IQueryable<User>>().Setup(m => m.GetEnumerator()).Returns(userData.GetEnumerator());
			userMockSet.Setup(m => m.Find(It.IsAny<object[]>())).Returns<object[]>(ids => userData.FirstOrDefault(d => d.Id == (int)ids[0]));

			mockContext.Setup(m => m.Users).Returns(userMockSet.Object);
			mockContext.Setup(m => m.Set<User>()).Returns(userMockSet.Object);
		}

		protected void StateInit()
		{
			//newState = new KanbanState { Name = "New" };
			//analyzedState = new KanbanState { Name = "Analyzed" };
			//inProgressState = new KanbanState { Name = "In progress" };
			//finishedState = new KanbanState { Name = "Finished" };

			typeof(KanbanState).GetProperty("Id").SetValue(newState, 0, null);
			typeof(KanbanState).GetProperty("Id").SetValue(analyzedState, 1, null);
			typeof(KanbanState).GetProperty("Id").SetValue(inProgressState, 2, null);
			typeof(KanbanState).GetProperty("Id").SetValue(finishedState, 3, null);

			//newState.NextState = analyzedState;
			//analyzedState.PreviousState = newState;
			//analyzedState.NextState = inProgressState;
			//inProgressState.PreviousState = analyzedState;
			//inProgressState.NextState = finishedState;
			//finishedState.PreviousState = inProgressState;

			newState.NextStateId = analyzedState.Id;
			analyzedState.PreviousStateId = newState.Id;
			analyzedState.NextStateId = inProgressState.Id;
			inProgressState.PreviousStateId = analyzedState.Id;
			inProgressState.NextStateId = finishedState.Id;
			finishedState.PreviousStateId = inProgressState.Id;

			stateData = new List<KanbanState> { newState, analyzedState, inProgressState, finishedState }.AsQueryable();

			stateMockSet.As<IQueryable<KanbanState>>().Setup(m => m.Provider).Returns(stateData.Provider);
			stateMockSet.As<IQueryable<KanbanState>>().Setup(m => m.Expression).Returns(stateData.Expression);
			stateMockSet.As<IQueryable<KanbanState>>().Setup(m => m.ElementType).Returns(stateData.ElementType);
			stateMockSet.As<IQueryable<KanbanState>>().Setup(m => m.GetEnumerator()).Returns(stateData.GetEnumerator());
			stateMockSet.Setup(m => m.Find(It.IsAny<object[]>())).Returns<object[]>(ids => stateData.FirstOrDefault(d => d.Id == (int)ids[0]));

			mockContext.Setup(m => m.KanbaanStates).Returns(stateMockSet.Object);
			mockContext.Setup(m => m.Set<KanbanState>()).Returns(stateMockSet.Object);
		}

		protected void TaskInit()
		{
			var list = new List<GtdTask>
			{
				new GtdTask { Name = "tt1", Description = "desc1" },
				new GtdTask { Name = "tt2", Description = "desc2" },
				new GtdTask { Name = "tt3", Description = "desc3" }
			};

			typeof(GtdTask).GetProperty("Id").SetValue(list[0], 0, null);
			typeof(GtdTask).GetProperty("Id").SetValue(list[1], 1, null);
			typeof(GtdTask).GetProperty("Id").SetValue(list[2], 2, null);

			list[0].KanbanStateId = newState.Id;
			list[1].KanbanStateId = inProgressState.Id;
			list[2].KanbanStateId = finishedState.Id;

			taskData = list.AsQueryable();

			taskMockSet.As<IQueryable<GtdTask>>().Setup(m => m.Provider).Returns(taskData.Provider);
			taskMockSet.As<IQueryable<GtdTask>>().Setup(m => m.Expression).Returns(taskData.Expression);
			taskMockSet.As<IQueryable<GtdTask>>().Setup(m => m.ElementType).Returns(taskData.ElementType);
			taskMockSet.As<IQueryable<GtdTask>>().Setup(m => m.GetEnumerator()).Returns(taskData.GetEnumerator());
			taskMockSet.Setup(m => m.Find(It.IsAny<object[]>())).Returns<object[]>(ids => taskData.FirstOrDefault(d => d.Id == (int)ids[0]));

			mockContext.Setup(m => m.GtdTasks).Returns(taskMockSet.Object);
			mockContext.Setup(m => m.Set<GtdTask>()).Returns(taskMockSet.Object);
		}
	}
}
