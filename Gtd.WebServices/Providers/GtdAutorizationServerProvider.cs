﻿using Gtd.Core.Services;
using Gtd.Infrastructure.Data;
using Gtd.WebServices.Services;
using Microsoft.Owin.Security.OAuth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;

namespace Gtd.WebServices.Providers
{
	public class GtdAutorizationServerProvider : OAuthAuthorizationServerProvider
	{
		public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext ctx)
		{
			ctx.Validated();
		}

		public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext ctx)
		{
			ctx.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { "*" });

			IUserService userService = new UserService(new GtdContext());
			if (!userService.AuthenticateUser(ctx.UserName, ctx.Password))
			{
				ctx.SetError("invalid_grant", "The user name or password is incorrect.");
				return;
			}

			var identity = new ClaimsIdentity(ctx.Options.AuthenticationType);
			identity.AddClaim(new Claim("sub", ctx.UserName));
			identity.AddClaim(new Claim("role", "user"));

			ctx.Validated(identity);
		}
	}
}