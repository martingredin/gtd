﻿using Gtd.Core.Model;
using Gtd.Core.Services;
using Gtd.Infrastructure.Data;
using Shared.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Gtd.WebServices.Services
{
	public class StateService : IStateService
	{
		GtdContext context;

		public StateService(GtdContext context)
		{
			this.context = context;
		}

		public KanbanState GetState(int stateId)
		{
			var stateRepo = new EFRepository<KanbanState>(context);
			return stateRepo.GetById(stateId);
		}

		public IEnumerable<KanbanState> GetAllStates()
		{
			var stateRepo = new EFRepository<KanbanState>(context);
			return stateRepo.List();
		}
	}
}