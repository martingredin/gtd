﻿using Gtd.Core.Services;
using Gtd.Infrastructure.Data;
using Gtd.WebServices.Models;
using Gtd.WebServices.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Gtd.WebServices.Controllers.Api
{
    public class StateController : ApiController
    {
		private IStateService stateService;

		public StateController()
		{
			stateService = new StateService(new GtdContext());
		}

		[HttpGet]
		[Authorize]
		[ActionName("GetAllStates")]
		public StateDto[] GetAllStates()
		{
			var allStates = stateService.GetAllStates();

			var dtos = new List<StateDto>();
			foreach (var state in allStates)
			{
				dtos.Add(new StateDto { Id = state.Id, Name = state.Name });
			}

			return dtos.ToArray();
		}
    }
}
