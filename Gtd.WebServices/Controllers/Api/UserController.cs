﻿using Gtd.Core.Services;
using Gtd.Infrastructure.Data;
using Gtd.Infrastructure.Repositories;
using Gtd.WebServices.Models;
using Gtd.WebServices.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Gtd.WebServices.Controllers
{
    public class UserController : ApiController
    {
		private IUserService userService;

		public UserController()
		{
			userService = new UserService(new GtdContext());
		}

		[HttpPost]
		[AllowAnonymous]
		[ActionName("Register")]
		public IHttpActionResult Register([FromBody]UserModelDto userModel)
		{
			if (!ModelState.IsValid)
				return BadRequest(ModelState);

			if (!userService.RegisterUser(userModel.UserId, userModel.Password, userModel.ConfirmPassword))
				return BadRequest();

			return Ok();
		}
    }
}