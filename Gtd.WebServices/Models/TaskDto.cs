﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Gtd.WebServices.Models
{
	public class TaskDto
	{
		public int Id { get; set; }
		public string UserName { get; set; }
		public string TaskName { get; set; }
		public string Description { get; set; }
		public string StartDate { get; set; }
		public string EndDate { get; set; }
	}
}