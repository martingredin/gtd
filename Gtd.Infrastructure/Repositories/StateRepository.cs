﻿using Gtd.Core.Model;
using Gtd.Core.Repositories;
using Gtd.Infrastructure.Data;
using Shared.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gtd.Infrastructure.Repositories
{
	public class StateRepository : EFRepository<KanbanState>, IStateRepository
	{
		protected GtdContext GtdContext
		{
			get { return (GtdContext)DataContext; }
		}

		public StateRepository(GtdContext context)
			: base(context)
		{

		}

		public KanbanState GetStateByName(string name)
		{
			return (from s in GtdContext.KanbaanStates
					where s.Name == name
					select s).FirstOrDefault();
		}
	}
}
