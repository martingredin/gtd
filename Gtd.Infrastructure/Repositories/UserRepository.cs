﻿using Gtd.Core.Model;
using Gtd.Core.Repositories;
using Gtd.Infrastructure.Data;
using Shared.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gtd.Infrastructure.Repositories
{
	public class UserRepository : EFRepository<User>, IUserRepository
	{
		protected GtdContext GtdContext
		{
			get { return (GtdContext)DataContext; }
		}

		public UserRepository(GtdContext context)
			: base(context)
		{

		}

		public User GetUser(string userName)
		{
			return (from u in GtdContext.Users
					where u.UserName == userName
					select u).FirstOrDefault();
		}
	}
}
