﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using Shared.Model;

namespace Gtd.Core.Model
{
	public class User : Entity<int>
	{
		public string UserName { get; set; }
		public string Hash { get; set; }
		public string Salt { get; set; }

		public virtual List<GtdTask> Tasks { get; set; }

		public User()
		{
			Tasks = new List<GtdTask>();
		}

		public static User Create(string userName, string password, string salt)
		{
			byte[] saltedHashBytes = Encoding.UTF8.GetBytes(password + salt);
			byte[] hashArray = new SHA256Managed().ComputeHash(saltedHashBytes);
		
			return new User { 
				UserName = userName, 
				Hash = Convert.ToBase64String(hashArray),
				Salt = salt
			};
		}

		public bool Authenticate(string userName, string password)
		{
			byte[] saltedHashBytes = Encoding.UTF8.GetBytes(password + Salt);
			byte[] hashArray = new SHA256Managed().ComputeHash(saltedHashBytes);
			string hash64 = Convert.ToBase64String(hashArray);

			if (UserName == userName && Hash == hash64)
				return true;

			return false;
		}
	}
}
