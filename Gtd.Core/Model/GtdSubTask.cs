﻿using Shared.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gtd.Core.Model
{
	public class GtdSubTask : Entity<int>
	{
		public string Name { get; set; }
		public string Description { get; set; }

		public int GtdTaskId { get; set; }
		public GtdTask GtdTask { get; set; }
	}
}
