﻿using Gtd.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gtd.Core.Services
{
	public interface ITaskService
	{
		GtdTask AddTask(string userName, string name, string description, DateTime? startDate, DateTime? endDate);

		GtdTask UpdateTask(int taskId, string newName, string newDescription);

		string MoveTaskToState(int taskId, Direction direction);

		void ChangeTaskToState(int taskId, int stateId);

		GtdTask GetTask(int taskId);

		IEnumerable<GtdTask> GetAllTasks();

		IEnumerable<GtdTask> GetTaskForUser(string userName);

		int DeleteTask(int taskId);
	}

	public enum Direction
	{
		Forwards = 1,
		Backwards = 0
	};
}
