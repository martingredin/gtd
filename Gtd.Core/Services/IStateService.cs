﻿using Gtd.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gtd.Core.Services
{
	public interface IStateService
	{
		KanbanState GetState(int stateId);

		IEnumerable<KanbanState> GetAllStates();
	}
}
