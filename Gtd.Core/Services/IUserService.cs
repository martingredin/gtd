﻿using Gtd.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gtd.Core.Services
{
	public interface IUserService
	{
		bool AuthenticateUser(string userName, string password);
		bool RegisterUser(string userName, string password, string confirmPassword);
		User GetUser(string userName);
	}
}
