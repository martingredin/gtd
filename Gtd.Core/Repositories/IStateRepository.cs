﻿using Gtd.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gtd.Core.Repositories
{
	public interface IStateRepository
	{
		KanbanState GetStateByName(string name);
	}
}
