﻿angular
    .module('GtdApp')
	.controller('StateManagementController', ['$scope', 'taskService', function ($scope, taskService) {
		$scope.tasks = taskService.getTasks();

		$scope.moveToNextState = function (taskId) {
			taskService.MoveTask(taskId, 1);
		}

		$scope.moveToPreviousState = function (taskId) {
			taskService.MoveTask(taskId, 0);
		}
	}]);