﻿angular
    .module('GtdApp')
	.controller('MainPageController', ['$scope', '$http', "$q", 'dialogService', 'authService', 'taskService',
		function ($scope, $http, $q, dialogService, authService, taskService) {
			$scope.tasks = taskService.getTasks();

			$scope.taskDto = new models.Task();

			$scope.newTaskName = "";
			$scope.newDescription = "";
			$scope.newStartDate = "";
			$scope.newEndDate = "";

			$scope.states = taskService.getStates();

			$scope.selectedState = {};

		$scope.createTask = function () {
			$scope.taskDto.UserName = authService.getUserName();
			$scope.taskDto.TaskName = $scope.newTaskName;
			$scope.taskDto.Description = $scope.newDescription;
			var startDateMoment = $('#startDatepicker').data("DateTimePicker").date();
			$scope.taskDto.StartDate = startDateMoment.year() + "-" + startDateMoment.month() + "-" + startDateMoment.date();
			var endDateMoment = $('#endDatepicker').data("DateTimePicker").date();
			$scope.taskDto.EndDate = endDateMoment.year() + "-" + endDateMoment.month() + "-" + endDateMoment.date();

			ClearRegTaskDlg();

			taskService.createTask($scope.taskDto);
			
			$('#taskRegDialog').modal('hide');
		}

		$scope.showEditTask = function (selectedId) {
			var taskToEdit = _.findWhere($scope.tasks, { id: selectedId });
			$scope.taskDto.Id = taskToEdit.id;
			$scope.taskDto.TaskName = taskToEdit.name;
			$scope.taskDto.Description = taskToEdit.description;
			$scope.taskDto.StateName = taskToEdit.state;
			$scope.selectedState = _.findWhere($scope.states, { name: taskToEdit.state });

			var startDateMoment = moment(taskToEdit.startDate);
			$('#editedStartDatepicker').data("DateTimePicker").date(startDateMoment);
			var endDateMoment = moment(taskToEdit.endDate);
			$('#editedEndDatepicker').data("DateTimePicker").date(endDateMoment);

			$('#taskEditDialog').modal('show');
		}

		$scope.editTask = function () {
			$scope.taskDto.UserName = authService.getUserName();
			$scope.taskDto.StateName = $scope.selectedState;
			taskService.editTask($scope.taskDto);
			$('#taskEditDialog').modal('hide');
		}

		$scope.deleteTask = function (selectedId) {
			taskService.deleteTask(selectedId);
		}

		function ClearEditTaskDlg() {
			$scope.taskDto = new models.Task();
		}

		function ClearRegTaskDlg() {
			$scope.newTaskName = "";
			$scope.newDescription = "";
			$scope.newStartDate = "";
			$scope.newEndDate = "";
			$('#startDatepicker').data('DateTimePicker').date(null);
			$('#endDatepicker').data('DateTimePicker').date(null);
		}

		$('#taskEditDialog').on('hidden.bs.modal', function () { ClearEditTaskDlg(); });
		$('#taskRegDialog').on('hidden.bs.modal', function () { ClearRegTaskDlg(); });
		$('#startDatepicker').datetimepicker({
			locale: 'en',
			format: 'MMM Do YYYY'
		});
		$('#endDatepicker').datetimepicker({
			locale: 'en',
			format: 'MMM Do YYYY'
		});
		$('#editedStartDatepicker').datetimepicker({
			locale: 'en',
			format: 'MMM Do YYYY'
		});
		$('#editedEndDatepicker').datetimepicker({
			locale: 'en',
			format: 'MMM Do YYYY'
		});
	}]);