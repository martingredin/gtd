﻿angular
	.module("Tasks", [])
	.factory("taskService", ["$http",  "$q", "cfg", "authService", "dialogService",
		function ($http, $q, cfg, authService, dialogService) {
		//variables
		var taskServiceFactory = {};

		var tasks = [];

		var states = [];

		//contruction
		$http
			.get(cfg.getTasksUrl, { params: { userName: authService.getUserName() } })
			.success(function (data) {
				if (!_.isEmpty(data)) {
					_.each(data, function (datum) {
						var task = new models.TaskList(datum);
						tasks.push(task);
					});
				}
			})
			.error(function (err, status) {
				dialogService.showDialog("Failed to get tasks", err.message);
			});

		$http
			.get(cfg.getAllStatesUrl)
			.success(function (data) {
				if (!_.isEmpty(data)) {
					_.each(data, function (datum) {
						var state = new models.State(datum);
						states.push(state);
					});
				}
			})
			.error(function (err, status) {
				dialogService.showDialog("Failed to get states", err.message);
			});

		//api
		taskServiceFactory.getTasks = function () {
			return tasks;
		};

		taskServiceFactory.getStates = function () {
			return states;
		};

		taskServiceFactory.createTask = function (taskDto) {
			$http
				.post(cfg.createTaskUrl, taskDto)
				.success(function (data) {
					var task = new models.TaskList(data);
					tasks.push(task);
				})
				.error(function (err, status) {
					dialogService.showDialog("Failed to create tasks", err.message);
				});
		};

		taskServiceFactory.editTask = function (taskDto) {
			$http
				.post(cfg.editTaskUrl, taskDto)
				.success(function (data) {
					var taskToUpdate = _.findWhere(tasks, { id: data.id });
					taskToUpdate.$merge(data);
				})
				.error(function (err, status) {
					dialogService.showDialog("Failed to edit tasks", err.message);
				});
		};

		taskServiceFactory.deleteTask = function (selectedId) {
			$http
				.post(cfg.deleteTaskUrl, selectedId)
				.success(function (data) {
					var idToRemove = _.map(tasks, function (e) { return e.id; }).indexOf(data);
					tasks.splice(idToRemove, 1);
				})
				.error(function (err, status) {
					dialogService.showDialog("Failed to delete tasks", err.message);
				});
		};

		taskServiceFactory.MoveTask = function (taskId, direction) {
			$http
				.post(cfg.moveTaskUrl, { TaskId: taskId, Direction: direction })
				.success(function (datum) {
					var taskToUpdate = _.findWhere(tasks, { id: datum.id });
					taskToUpdate.state = datum.state;
				})
				.error(function (err, status) {
					dialogService.showDialog("Failed to delete tasks", err.message);
				});
		};

		return taskServiceFactory;
	}]);