﻿angular
	.module('Dialogs', [])
	.factory('dialogService', ['$rootScope', '$compile', function ($rootScope, $compile) {
		var service = {};

		var htmlTemplate = [
			"<div class='modal fade' id='authErrorDialog' tabindex='-1' role='dialog' aria-labelledby='basicModal' aria-hidden='true'>",
				"<div class='modal-dialog'>",
					"<div class='modal-content'>",
						"<div class='modal-header'>",
							"<h3 class='modal-title'>{{ headerText }}</h3>",
						"</div>",
						"<div class='modal-body'>",
							"<h4>{{ bodyText }}</h4>",
						"</div>",
						"<div class='modal-footer'>",
							"<button type='button' class='btn btn-primary' data-dismiss='modal'>",
								"<span class='glyphicon glyphicon-remove'></span>Close",
							"</button>",
						"</div>",
					"</div>",
				"</div>",
			"</div>"].join("");

		var scope = $rootScope.$new();
			
		var htmlDialog = $compile(htmlTemplate)(scope);

		service.showDialog = function (header, body) {
			scope.headerText = header;
			scope.bodyText = body;
			$(htmlDialog).modal('show');
		}

		return service;
	}]);